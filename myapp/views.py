from django.shortcuts import render, redirect
from . import forms
from .models import Status

def landing(request):
    if request.method == "POST":
        form = forms.StatusForms(request.POST)
        if form.is_valid():
            form.save()
            return redirect('landing')
    else:
        form = forms.StatusForms()
    status = Status.objects.all()
    return render (request, 'landing.html', {'form' : form, 'status' : status})
