from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.apps import apps
from .models import Status
from .forms import StatusForms
from .views import landing
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time


class StorySixUnitTest(TestCase):

    def test_url_is_exist(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_urls_is_not_exist(self):
        response = self.client.get('/')
        self.assertFalse(response.status_code==404)

    def test_index_contains_greeting(self):
        response = self.client.get('/')
        response_content = response.content.decode('utf-8')
        self.assertIn("Hello There! Write Something Here.", response_content)

    def create_status(self):
        new_status = Status.objects.create(status = "Coba Status")
        return new_status

    def test_check_status(self):
        c = self.create_status()
        self.assertTrue(isinstance(c, Status))
        self.assertTrue(c.__str__(), c.status)
        counting_all_status = Status.objects.all().count()
        self.assertEqual(counting_all_status, 1)

    def test_form(self):
        form_data = {
        'status' : 'ini adalah sebuah status',
        }
        form = StatusForms(data = form_data)
        self.assertTrue(form.is_valid())
        request = self.client.post('/', data = form_data)
        self.assertEqual(request.status_code, 302)

        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)


class StorySixFunctionalTest(LiveServerTestCase):

    def setUp(self):
        # chrome
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.browser = webdriver.Chrome('./chromedriver',chrome_options=chrome_options)
        super(StorySixFunctionalTest, self).setUp()

    def tearDown(self):
        self.browser.refresh()
        self.browser.quit()
        super(StorySixFunctionalTest, self).tearDown()

    def test_post(self):
        self.browser.get(self.live_server_url)
        new_status = self.browser.find_element_by_id('id_status')
        new_status.send_keys("cobain aja")
        time.sleep(3)
        submit = self.browser.find_element_by_id('submit')
        submit.send_keys(Keys.RETURN)
        time.sleep(2)
        self.browser.get(self.live_server_url)
        time.sleep(2)
        self.assertIn('cobain aja', self.browser.page_source)